package database.mappings

import slick.jdbc.H2Profile.api._

/**  Case class of Login Table Row */

case class InsertRow(
  user: String,
  candy: String,
  coinValue: Double,
  timeStamp: Long)

/**
 * Class that defines the login table, making username a foreign key in the database
 * @param tag slick tag
 */
class InsertTable(tag: Tag) extends Table[InsertRow](tag, _tableName = "inserts") {
  def user: Rep[String] = column[String]("user")
  def candy: Rep[String] = column[String]("candy")
  def coinValue: Rep[Double] = column[Double]("coinValue")
  def timeStamp: Rep[Long] = column[Long]("timeStamp")

  def * = (user, candy, coinValue, timeStamp) <> (InsertRow.tupled, InsertRow.unapply)
}

/** Queries of user table and login table */
object EntryMappings {
  lazy val insertTable = TableQuery[InsertTable]
}
