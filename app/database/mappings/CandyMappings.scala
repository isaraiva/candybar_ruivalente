package database.mappings

import slick.jdbc.H2Profile.api._

/**  Case class of Candy Table Row */

case class CandyRow(
  candy: String,
  price: Double)

/**
 * Class that defines the candy table
 * @param tag slick tag
 */
class CandyTable(tag: Tag) extends Table[CandyRow](tag, _tableName = "candy") {
  def candy: Rep[String] = column[String]("candy")
  def price: Rep[Double] = column[Double]("price")

  def * = (candy, price) <> (CandyRow.tupled, CandyRow.unapply)
}

/** Queries of candy table */
object CandyMappings {
  lazy val candyTable = TableQuery[CandyTable]
}
