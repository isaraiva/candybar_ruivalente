package database.repository

import api.dtos.CreateEntryDTO
import database.mappings.CandyMappings._
import database.mappings.EntryMappings._
import database.mappings.{ CandyRow, InsertRow }
import database.properties.DBProperties
import javax.inject.Inject
import slick.jdbc.H2Profile.api._

import scala.concurrent.{ ExecutionContext, Future }

/**  Class that receives a db path */
class InsertRepositoryImpl @Inject() (dbClass: DBProperties)(implicit val executionContext: ExecutionContext) extends InsertRepository {

  val db = dbClass.db

  /**
   * Insert an user into database with is password encrypted
   * @return The number of insertions into database
   */
  def insertCoin(candy: String, coin: CreateEntryDTO): Future[Double] = {

    val timeOfInsertion = System.currentTimeMillis()

    //TODO: There should be verified if the candy requested is available,
    // otherwise this row should not be inserted
    db.run(insertTable += InsertRow(coin.id, candy, coin.value, timeOfInsertion))

    val sumCoins = candyTable
      .filter(_.candy === candy)
      .join(insertTable
        .filter(_.timeStamp >= timeOfInsertion - 180000)
        .filter(_.user === coin.id)
        .filter(_.candy === candy))
      .on(_.candy === _.candy)
      .map(_._2.coinValue)
      .result

    db.run(sumCoins).map(x => if (x.isEmpty) -1
    else x.sum)
  }

  //verify if there are 5 users in the database already beyond coin.user
  def userVerification(user: String): Future[Int] = {
    val insertTableEntry = insertTable.map(_.user).distinct.result
    db.run(insertTableEntry).map(seq => (seq :+ user).toSet.size)
  }

  def candyBuy(candy: String, sum: Double, coin: CreateEntryDTO): Future[Double] = {

    val timeOfInsertion = System.currentTimeMillis()

    val price = db.run(candyTable
      .filter(_.candy === candy)
      .map(_.price).result)

    price.flatMap { seqPrice =>
      seqPrice.headOption.getOrElse(0.0) < sum match {
        //Candy status is deleted from the stock table
        case true =>
          val res = for {
            _ <- db.run(candyTable
              .filter(_.candy === candy)
              .delete)
            //Delete the rows used to buy the candy
            _ <- db.run(insertTable
              .filter(_.timeStamp >= timeOfInsertion - 180000)
              .filter(_.user === coin.id)
              .filter(_.candy === candy)
              .delete)
          } yield 0

          res.map(x => x + sum - seqPrice.headOption.getOrElse(0.0))

        //In case the user had bought the candy, it is returned a positive value with the change
        //Candy not sold, return negative number that refers to quantity missing
        case false => Future.successful { sum - seqPrice.headOption.getOrElse(0.0) }
      }
    }
  }

  def deleteOldRows: Future[Int] = {
    val timeOfInsertion = System.currentTimeMillis()
    val oldRows = insertTable
      .filter(_.timeStamp < timeOfInsertion - 180000)
      .delete
    db.run(oldRows)
  }

  def insertCandies: Future[Seq[Int]] = {

    val candySeq = Seq(
      CreateEntryDTO("Mars", 2.80),
      CreateEntryDTO("Snickers", 2.80),
      CreateEntryDTO("KitKat", 2.80),
      CreateEntryDTO("Bounty", 2.80))

    Future.sequence { candySeq.map(candy => db.run(candyTable += CandyRow(candy.id, candy.value))) }
  }
}
