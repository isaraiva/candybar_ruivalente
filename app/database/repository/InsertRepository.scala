package database.repository

import api.dtos.CreateEntryDTO

import scala.concurrent.Future

trait InsertRepository {

  def insertCoin(candy: String, coin: CreateEntryDTO): Future[Double]
  def userVerification(user: String): Future[Int]
  def candyBuy(candy: String, sum: Double, coin: CreateEntryDTO): Future[Double]
  def deleteOldRows: Future[Int]
  def insertCandies: Future[Seq[Int]]
}
