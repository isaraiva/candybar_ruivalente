package database

import database.mappings.CandyMappings.candyTable
import database.mappings.EntryMappings.insertTable
import database.properties.DBProperties
import slick.jdbc.H2Profile.api._
import javax.inject.Inject
import javax.inject.Singleton

import scala.concurrent.Await
import scala.concurrent.duration.Duration

@Singleton
class CreateTables @Inject() (dbClass: DBProperties) {

  val db = dbClass.db

  val setupAction = DBIO.seq(
    sqlu"DROP TABLE IF EXISTS #${candyTable.baseTableRow.tableName}",
    sqlu"DROP TABLE IF EXISTS #${insertTable.baseTableRow.tableName}")

  Await.result(db.run(setupAction), Duration.Inf)
  val tables = Seq(candyTable, insertTable)
  Await.result(db.run(DBIO.seq(tables.map(_.schema.create): _*)), Duration.Inf)

}
