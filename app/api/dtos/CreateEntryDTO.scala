package api.dtos
import play.api.libs.json.{ Json, OFormat }

case class CreateEntryDTO(
  id: String,
  value: Double)

object CreateEntryDTO {
  implicit val createEntryDTO: OFormat[CreateEntryDTO] = Json.format[CreateEntryDTO]
}