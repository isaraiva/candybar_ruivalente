package api.controllers

import akka.actor.ActorSystem
import api.dtos.CreateEntryDTO
import database.repository.InsertRepository
import javax.inject._
import play.api.libs.json._
import play.api.mvc._

import scala.concurrent.{ ExecutionContext, Future }

/** Class that is injected with end-points */

@Singleton class EntryController @Inject() (
  cc: ControllerComponents,
  actorSystem: ActorSystem,
  insertCoinActions: InsertRepository)(implicit exec: ExecutionContext)

  extends AbstractController(cc) {

  /*
    * TODO: Move verification to DTO
   */
  private def coinValidation(coin: Double): Boolean = {
    val listOfAllowedCoins: List[Double] = List(0.25, 0.5, 1.0)
    listOfAllowedCoins.contains(coin)
  }

  /**
   * Sign in action
   *
   * @return When a valid user is inserted, it is added in the database, otherwise an error message is sent
   */
  def InsertCoin(candy: String): Action[JsValue] = Action(parse.json).async { request: Request[JsValue] =>
    val entryResult = request.body.validate[CreateEntryDTO]

    entryResult.fold(
      errors => {
        insertCoinActions.deleteOldRows
        Future {
          BadRequest("Error in coin insertion")
        }
      },
      entry => {
        coinValidation(entry.value) match {
          case true => insertCoinActions.userVerification(entry.id).flatMap {
            case numberEntries if numberEntries <= 5 =>
              insertCoinActions.insertCoin(candy, entry).flatMap { sumDollars =>
                if (sumDollars >= 0) {
                  insertCoinActions.candyBuy(candy, sumDollars, entry).map {
                    case a if a > 0 =>
                      insertCoinActions.deleteOldRows
                      Ok("Drop the candy. The change is " + math.round(a * 100).toDouble / 100 + " $")
                    case b =>
                      insertCoinActions.deleteOldRows
                      Ok("Need to insert " + math.round(-b * 100).toDouble / 100 + "$ more")
                  }
                } else Future {
                  insertCoinActions.deleteOldRows
                  BadRequest("Request not valid")
                }
              }
            case _ => Future {
              insertCoinActions.deleteOldRows
              BadRequest("Coin not valid")
            }
          }
          case _ => Future {
            BadRequest("User not valid")
          }
        }
      })
  }

  def Insert4Candies: Action[AnyContent] = Action.async {
    insertCoinActions.insertCandies
    Future { Ok("Candies inserted") }
  }
}
