import com.google.inject.AbstractModule
import database.CreateTables
import database.properties.TestDBProperties
import database.repository._

import scala.concurrent.ExecutionContext

/** Outline the database to be used implicitly */
class Module extends AbstractModule {

  override def configure(): Unit = {

    implicit val ec: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global

    bind(classOf[CreateTables]).toInstance(new CreateTables(TestDBProperties))
    bind(classOf[InsertRepository]).toInstance(new InsertRepositoryImpl(TestDBProperties))

  }
}