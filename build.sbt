name := """play-scala-starter-example"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

resolvers += Resolver.sonatypeRepo(status ="snapshots")

scalaVersion := "2.12.7"

libraryDependencies ++= Seq(
  guice,
  "org.scalatestplus.play"  %% "scalatestplus-play"     % "3.1.2",
  "com.h2database"          % "h2" % "1.4.197",
  "com.typesafe.slick"      %% "slick-hikaricp"         % "3.2.0",
)